package com.example.cineaste

class MovieListViewModel {
    fun getFavoriteMovies():List<Movie> {
        return MovieRepository.getFavoriteMovies()
    }
    fun getRecentMovies(): List<Movie> {
        return MovieRepository.getRecentMovies()
    }
}